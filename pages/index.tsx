import type { NextPage } from "next";
import Link from "next/link";
import styled from "styled-components";

const Root = styled.div`
  width: 100%;
  max-width: 1000px;
  margin: auto;
  padding: 20px;

  a {
    color: lightblue;
    text-decoration: underline;

    &::after {
      content: " 🔗";
    }
  }
`;

const Home: NextPage = () => {
  return (
    <Root>
      <h1>Checkout all the demos</h1>
      <ul>
        <li>
          <Link href="/sticky-header">
            Sticky headers
          </Link>
        </li>
        <li>
          <Link href="/json-to-table">
            JSON to table
          </Link>
        </li>
      </ul>
    </Root>
  );
};

export default Home;
