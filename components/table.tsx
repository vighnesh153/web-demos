import React from "react";
import styled from "styled-components";

const Container = styled.div`
  table {
    border-collapse: collapse;

    th,
    td {
      padding: 10px 20px;

      white-space: pre;
    }
  }
`;

interface TableProps {
  rows: any[];
  onEmpty?: JSX.Element;
}

export default function Table({ rows, onEmpty }: TableProps): JSX.Element {
  if (rows.length === 0) {
    return onEmpty ?? <p>No rows found</p>;
  }

  return (
    <Container>
      <table border={1}>
        <thead>
          <tr>
            {Object.keys(rows[0]).map((key) => (
              <th key={key}>{key}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {rows.map((row, index) => (
            <tr key={index}>
              {Object.keys(row).map((key) => {
                const value = row[key];
                if (Array.isArray(value)) {
                  return <Table key={key} rows={value} onEmpty={<td>[]</td>} />;
                }
                return <td key={key}>{JSON.stringify(value, null, 2)}</td>;
              })}
            </tr>
          ))}
        </tbody>
      </table>
    </Container>
  );
}
