import { useState } from "react";
import styled from "styled-components";

import Table from "./table";

const Root = styled.div`
  width: 100%;
  max-width: 1500px;
  margin: auto;
  padding: 20px;

  overflow: auto;
`;

export default function JsonToTable() {
  const [jsonValue, setJsonValue] = useState("");
  const [key, setKey] = useState("");
  const [tableValue, setTableValue] = useState<any[]>([]);

  const calculateTable = () => {
    try {
      const evaluatedJson = JSON.parse(jsonValue);

      const keySegments = key.split(".");
      setTableValue(
        keySegments.reduce((previous, keySegment) => {
          return previous[keySegment];
        }, evaluatedJson) as any[]
      );
    } catch (e) {
      alert((e as any).message);
    }
  };

  const spacer = (
    <>
      <br />
      <br />
      <br />
    </>
  );

  return (
    <Root>
      <h1>JSON to Table</h1>

      <label>
        Enter JSON value
        <br />
        <br />
        <input
          type="text"
          style={{ width: "100%" }}
          value={jsonValue}
          onChange={(e) => setJsonValue(e.target.value)}
        />
      </label>

      {spacer}

      <label>
        Enter key
        <br />
        <br />
        <input
          type="text"
          style={{ width: "100%" }}
          value={key}
          onChange={(e) => setKey(e.target.value)}
        />
      </label>

      {spacer}

      <button onClick={calculateTable}>Draw table</button>

      {spacer}

      <Table rows={tableValue} />
    </Root>
  );
}
