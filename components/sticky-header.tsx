import styled from "styled-components";

const items = [
  {
    month: "October 2022",
    activities: [
      new Date("10-20-2022"),
      new Date("10-12-2022"),
      new Date("10-08-2022"),
      new Date("10-02-2022"),
    ],
  },
  {
    month: "September 2022",
    activities: [
      new Date("09-29-2022"),
      new Date("09-13-2022"),
      new Date("09-10-2022"),
      new Date("09-09-2022"),
      new Date("09-08-2022"),
      new Date("09-07-2022"),
      new Date("09-02-2022"),
    ],
  },
  {
    month: "August 2022",
    activities: [
      new Date("08-29-2022"),
      new Date("08-13-2022"),
      new Date("08-10-2022"),
      new Date("08-09-2022"),
      new Date("08-08-2022"),
      new Date("08-07-2022"),
      new Date("08-02-2022"),
    ],
  },
];

const Root = styled.div`
  width: 100%;
  max-width: 1000px;
  margin: auto;
  padding: 20px;
`;

const MonthContainer = styled.section`
  margin-bottom: 20px;
  padding-bottom: 20px;

  display: flex;
  align-items: flex-start;

  border-bottom: 2px solid grey;

  &:last-of-type {
    padding-bottom: 0;

    border: none;
  }

  & > * {
    flex-basis: 50%;
  }

  .month-title {
    position: sticky;
    top: 20px;
  }

  .month-activities {
    display: flex;
    flex-direction: column;
    gap: 20px;
  }

  .month-activity {
    padding: 20px 40px;
    height: 70px;

    border: 1px solid grey;
    border-radius: 10px;
  }
`;

export default function StickyHeader() {
  return (
    <Root>
      <h1>Sticky Header Demo</h1>
      <br />

      {items.map((item) => (
        <MonthContainer key={item.month}>
          <h2 className="month-title">{item.month}</h2>
          <div className="month-activities">
            {item.activities.map((activity, index) => (
              <div key={index.toString()} className="month-activity">
                Activity {activity.toDateString()}
              </div>
            ))}
          </div>
        </MonthContainer>
      ))}

      <div style={{ height: 900 }} />
    </Root>
  );
}
